import java.io.*;

/**
 * The logger is in charge of writing output to 'cron.log'. It does this in its own thread, but
 * assumes that other threads will call the setMessage() in order to provide messages to log. (You
 * need to fill in the details!)
 */
public class Logger {
    private String nextMessage = "";
    private Thread thread;
    private Object monitor = new Object();

    public void setMessage(String newMessage) throws InterruptedException {
        synchronized (monitor) {
            // If an 'if-statement' is used here instead of 'while',
            // only one check will be performed and so, nextMessage will not be set.
            while (nextMessage != "") {
                monitor.wait();
            }
            nextMessage = newMessage;
            // When we are done setting next message, we notify all threads waiting for the locked resource
            // (in this case 'monitor'), we are basically saying "Hey you threads are free to use it now!"
            monitor.notifyAll();
        }
    }


    public void start() {
        Runnable myTask = new Runnable()
        {
            @Override
            public void run() {
                // Some standard order for the monitor you should remember.
                // IMPORTANT: The monitor 'synchronized, try, while' order is DIFFERENT from the normal thread :)
                // Compare this against Scheduler.run()
                synchronized (monitor) {
                    try {
                        while (true) {
                            // Here, since we are already looping with while(true), we can just use if for monitor.wait()
                            // Checking to see if nextMessage is empty (default state), if it is we just wait......
                            if (nextMessage == "") {
                                monitor.wait();
                            }

                            // Yay, nextMessage is no longer empty here, so we just append the message to the file cron.log
                            try (PrintWriter writer =
                                         new PrintWriter(new FileWriter("cron.log", true))) {
                                writer.println(nextMessage);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }

                            // When we are done, we clean up the message so the next iteration doesn't append the same message twice
                            nextMessage = "";
                            // We notify all threads waiting for the resources that they can use this class now
                            // Specifically here, setMessage() function is notified that nextMessage is empty, you can
                            // set it now.
                            monitor.notifyAll();
                        }
                    } catch (InterruptedException e) {
                        System.out.println("Logger closed");
                    }
                }
            }
        };
        thread = new Thread(myTask, "Logger Thread");
        // Start thread.
        thread.start();
    }

    public void stop() {
        thread.interrupt();
    }
}
