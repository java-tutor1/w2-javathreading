import java.util.*;

/**
 * The scheduler keeps track of all the jobs, and runs each one at the appropriate time. (You need
 * to fill in the details!)
 */
public class Scheduler {
    private ArrayList<Job> jobs = new ArrayList<>();
    private Thread thread;
    private Timer timer = new Timer();
    private Object mutex = new Object();
    private int prevSize = 0;

    public void addJob(Job newJob) {
        synchronized (mutex) {
            jobs.add(newJob);
        }
    }

    public void start() {
//        /// METHOD 1: Simple way of not using Java Timer class
//        Runnable myTask = new Runnable() {
//            @Override
//            public void run() {
//                int threadTimer = 0;
//                try {
//                    // The scheduler runs continuously throughout the program,
//                    // so we want to continuously loop to check for any changes.
//                    while (true) {
//                        synchronized (mutex) {
//                            // Continuously loop through the list of jobs, check the current thread timer
//                            // against the job's delay.
//                            for (Job job : jobs) {
//                                if (threadTimer % job.delay == 0) {
//                                    Thread t = new Thread(job);
//                                    t.start();
//                                }
//                            }
//                            threadTimer++;
//                            thread.sleep(1000);
//                        }
//                    }
//                } catch (InterruptedException e) {
//                    System.out.println("Stopped");
//                }
//            }
//        };
//        thread = new Thread(myTask, "Scheduler Thread"); // Start thread.
//        thread.start();


        /// METHOD 2: The other more hardcore way :)
        Runnable myTask = new Runnable() {
            @Override
            public void run() {
                // The scheduler runs continuously throughout the program,
                // so we want to continuously loop to check for any changes.
                // IMPORTANT: The monitor 'synchronized, try, while' order is DIFFERENT from the normal thread :)
                // Compare this against Logger.run()
                while (true) {
                    synchronized (mutex) {
                        // Only start a new timed task when a new job is added to the jobs list
                        if (jobs.size() != prevSize) {
                            // Get the job at the last index
                            Job job = jobs.get(jobs.size() - 1);
                            TimerTask timedTask = new TimerTask() {
                                @Override
                                public void run() {
                                    Thread t = new Thread(job);
                                    t.start();
                                }
                            };
                            long delay = job.delay * 1000;
                            // We start the timed task here
                            timer.scheduleAtFixedRate(timedTask, delay, delay);
                            // Update the previous size
                            prevSize = jobs.size();
                        }
                    }
                }
            }
        };
        thread = new Thread(myTask, "Scheduler Thread");
        // Start thread.
        thread.start();
    }

    public void stop() {
        thread.interrupt();
        // A must-have for METHOD 2, used to discard all scheduled tasks
        timer.cancel();
    }
}
